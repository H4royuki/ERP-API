package com.developer.ERP.Legacy.API.core.validation.message;

public class ClienteMessage {
	public final static String MSG_INFORME_CADASTRO_CLIENTE = "Porfavor informe no cadsatro de cliente se você é uma pessoa fisica ou juridica.";
	public final static String MSG_CLIENTE_NAO_ENCONTRADO ="Não foi possivel encontrar esse recurso.";
	public final static String MSG_CLIENTE_OUTROS_NAO_INFORMADO = "Para prosseguir deve ser informado o cadastro de terceiro.";
	public final static String MSG_CLIENTE_ERRO_INTERNO = "Ocorreu um erro ao tentar salvar o cliente.";
	public final static String MSG_CLIENTE_AGENCIA_NAO_INFORMADA = "Informe no cadastro do cliente a agência para prosseguir.";
	public final static String MSG_CLIENTE_CONTA_BANCO_NAO_INFORMADO = "Informe no cadastro do cliente o banco para prosseguir.";
	public final static String MSG_CLIENTE_FORMA_PAGAMENTO_NAO_INFORMADO = "Informe no cadastro do cliente a forma de pagamento para prosseguir.";
	public final static String MSG_CLIENTE_ERRO_ADICIONAR_REFERENCIAS_COMERCIAIS = "Não foi possivel adicionar referências comerciais para esse cliente erro atual:";
	public final static String MSG_CLIENTE_ERRO_ADICIONAR_ENDERECO_CLIENTE = "Não foi possivel adicionar o endereço atual ao cliente erro atual:";
	public final static String MSG_CLIENTE_CONTRATOS_EXPIRADOS = "Não foi possivel remover o cliente atual pois o mesmo possui contratos expirados, verificar os contratos expirados e renovar.";
	public final static String MSG_CLIENTE_ERRO_REMOVER = "Não foi possivel remover o cliente atual.Pois está em uso";

	public final static String MSG_CLIENTE_CADASTRO_JA_EXISTE = "O cliente que está tentando salvar não pode ser salvo pois o mesmo, possui terceiros vinculados ou o cadastro já existente";
	
}
