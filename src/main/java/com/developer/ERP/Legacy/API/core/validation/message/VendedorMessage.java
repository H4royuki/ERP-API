package com.developer.ERP.Legacy.API.core.validation.message;

public class VendedorMessage {
    public final static String MSG_INFORMATION = "campo não pode ser nulo";
    public final static String MSG_INFORMATION_NOT_FOUND_VENDEDOR = "Vendedor não foi localizado com esse id:";
}
