package com.developer.ERP.Legacy.API.domain.enumerated;

public enum Estado {
	CE,
	PE,
	RJ,
	SP,
	MA
}
