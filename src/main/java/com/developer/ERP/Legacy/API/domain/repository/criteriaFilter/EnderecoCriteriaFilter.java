package com.developer.ERP.Legacy.API.domain.repository.criteriaFilter;

import lombok.Data;

@Data
public class EnderecoCriteriaFilter {
	private String cep;
	private String bairro;
}
